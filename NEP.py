import fitz, sys, pathlib
import nltk
from nltk.tree import Tree
import re
from copy import deepcopy

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')

#document = "I volume 18 m × 18 m × 18 m another one 19 m3 and 20 m × 20 m and the last 2 m 2 m"
text_file = open("parsed3.txt", mode="r", encoding="utf-8")
document = text_file.read() # open a document
document = "absorbing sample is 10:8 m2 3 m 3:6 m or our method was designed."

text_file.close()
sentences = nltk.pos_tag(nltk.word_tokenize(document))

grammar = r"""
	NUMBER: {<CD><NN|VBD>}
	METHOD: {<NN><VB.*>+}
	NP: {<METHOD> | <NUMBER><VBD>?<NUMBER><VBD>?<NUMBER> | <NUMBER><VBD?><NUMBER> | <NUMBER>+ }
"""  #1800 m3 parsed

p = nltk.RegexpParser(grammar); 
res = p.parse(sentences)

print(res)

parse_result = open("parse_result3.txt", "w", encoding="utf-8")

m_subtrees = res.subtrees(filter=lambda t: t.label() == 'NP' and 'm' in [child[0] for child in t.leaves()])

for subtree in m_subtrees:
	p = str(subtree)
	#cleaning string
	p = re.sub(r'[^\w\s,.:()\/\\]', '', p)
	parse_result.write(p)
	
np_trees = res.subtrees(filter=lambda t: t.label() == 'NP' and 'sample' in [child[0] for child in t.leaves()]
or 'method' in [child[0] for child in t.leaves()]) 

np_trees_unique = []
for subtree in np_trees:
    if (subtree.label() == 'NP' and ('sample' in [child[0] for child in subtree.leaves()]
        or 'method' in [child[0] for child in subtree.leaves()])):
        np_trees_unique.append(deepcopy(subtree))

for subtree in np_trees_unique:
	p = str(subtree)
	parse_result.write(p)
	
parse_result.close()
