import spacy
from spacy.matcher import Matcher
import fitz, sys, pathlib

nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)
# Add match ID "HelloWorld" with no callback and one pattern
patterns = [
    [{"LOWER": "hello"}, {"IS_PUNCT": True}, {"LOWER": "world"}],
    [{"LOWER": "hello"}, {"LOWER": "world"}]
]
matcher.add("HelloWorld", patterns)

raw = fitz.open('sample2.pdf')
text = "";
for page in raw: # iterate the document pages
    text += page.get_text() # get plain text encoded as UTF-8

doc = nlp(text)
matches = matcher(doc)
print(len(matches), "\n")

for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print(match_id, string_id, start, end, span.text)

print("------------\n")

doc = nlp("Hello World")
matches = matcher(doc)
print(len(matches), "\n")

for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print(match_id, string_id, start, end, span.text)