from __future__ import unicode_literals
from parglare import Grammar, Parser
import difflib

grammar = """
S: Element+;
Element: Bar | Baz | Number;

terminals
Bar: /Bar. \d+/;
Baz: /Baz. \d+/;
Number: /\d+/;
"""

g = Grammar.from_string(grammar)
grammar = [g]

def custom_token_recognition(context, get_tokens):
    """
    Custom token recognition should return a single token that is
    recognized at the given place in the input string.
    """
    # Call default token recognition.
    tokens = get_tokens()

    if tokens:
        # If default recognition succeeds use the result.
        return tokens
    else:
        # If no tokens are found do the fuzzy match.
        matchers = [
            lambda x: difflib.SequenceMatcher(None, 'bar.', x.lower()),
            lambda x: difflib.SequenceMatcher(None, 'baz.', x.lower())
        ]
        symbols = [
            grammar[0].get_terminal('Bar'),
            grammar[0].get_terminal('Baz'),
        ]
        # Try to do fuzzy match at the position
        elem = context.input_str[context.position:context.position+4]
        elem_num = context.input_str[context.position:]
        number_matcher = re.compile('[^\d]*(\d+)')
        number_match = number_matcher.match(elem_num)
        ratios = []
        for matcher in matchers:
            ratios.append(matcher(elem).ratio())

        max_ratio_index = ratios.index(max(ratios))
        if ratios[max_ratio_index] > 0.7 and number_match.group(1):
            return [Token(symbols[max_ratio_index], number_match.group())]


parser = Parser(
    g, custom_token_recognition=custom_token_recognition)


# Bar and Baz will be recognized by a fuzzy match
result = parser.parse('bar. 56 Baz 12')
assert result == ['bar. 56', 'Baz 12']

result = parser.parse('Buz. 34 bar 56')
assert result == ['Buz. 34', 'bar 56']

result = parser.parse('Ba. 34 baz 56')
assert result == ['Ba. 34', 'baz 56']

# But if Bar/Baz are too different from the correct pattern
# we get ParseError. In this case `bza` score is below 0.7
# for both Bar and Baz symbols.
with pytest.raises(ParseError):
    parser.parse('Bar. 34 bza 56')