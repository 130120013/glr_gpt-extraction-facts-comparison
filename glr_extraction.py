from __future__ import unicode_literals
from parglare import Grammar, GLRParser
import fitz, sys, pathlib

grammar = r"""
S: Size | EMPTY;

Size: Number WS? "m3";

terminals
WS: /\s+/;
Number: /\d+(\.\d+)?/;
"""


def main(debug=False):
    g = Grammar.from_string(grammar)
    pr = GLRParser(g, ws='\t ', debug=debug, debug_colors=True)

    input_str = "asfa 2 m3"

    res = pr.parse(input_str)

    print("Input:\n", input_str)
    print("Result = ", res)
    print(len(res), "\n")
    print(res.solutions, "\n")
    print(res.ambiguities, "\n")
    print(res.to_str())

    #raw = fitz.open('sample2.pdf')
    
    #print('size - {}\n'.format( len(raw)))
    #text = "";
    #for page in raw: # iterate the document pages
    #    text += page.get_text() # get plain text encoded as UTF-8

    #print("\n")
    #pathlib.Path("parsed2.txt").write_bytes(text.encode())
    #raw.save("parsed1.txt")
    #raw = fitz.open('sample2.pdf')
    #for page in raw: # iterate the document pages
    #    print(page.get_text(), "\n") # get plain text encoded as UTF-8

if __name__ == "__main__":
    main(debug=True)