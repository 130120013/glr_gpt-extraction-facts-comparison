﻿from copy import deepcopy
import os
from xml import dom
import nltk
import DictionaryManager
import time
import RuleExecutor
from NLUtils import stem
import PdfConverter
import re
import string

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

def write_trees(trees, file):
    for subtree in trees:
        file.write(str(subtree))

def write_domain_entities(domain_entities, file):
    for en in domain_entities:
        file.write("\n")
        file.write(en[0]) # processed rule
        
        for subtree in en[1]:
            file.write(str(subtree))

def parse_abstract(document):
    tokens = nltk.word_tokenize(document)
    processed_tokens = stem(tokens)
    begin_abstract_p = 'abstract' in processed_tokens
    if not begin_abstract_p:
        return 0, 0

    abstract_pos = processed_tokens.index('abstract')

    end_abstract_p = 'introduct' in processed_tokens
    if not end_abstract_p:
        return 0, 0

    end_abstract_pos = processed_tokens.index('introduct')
    abstract = processed_tokens[abstract_pos:end_abstract_pos]

    sentences = nltk.pos_tag(abstract)

    grammar = r"""
        BASE: {<DT>?<NN.*><VB.*>}
	    PRESENT: {<BASE>+<DT>?<NN.*|VB.*|JJ.*>?}
        PROPOSE: {<PRP><VB.*>*<NN.*>} 
	    NP: { <PRESENT> | <PROPOSE> }
    """
    p = nltk.RegexpParser(grammar)
    res = p.parse(sentences)

    abstract_parse_res = []

    for subtree in res.subtrees(filter=lambda t: t.label() == 'NP'):
        abstract_parse_res.append(str(subtree))

    return abstract_pos, end_abstract_pos, abstract_parse_res


# document - opened document as string
def parse_conclusion(document):
    tokens = nltk.word_tokenize(document)
    processed_tokens = stem(tokens)
    a = 'conclus' in processed_tokens
    if not a:
        return 0, 0

    conclusion_pos = processed_tokens.index('conclus')
    conclusion = processed_tokens[conclusion_pos:len(processed_tokens)]
    conclusion_end_pos = 'fund' in processed_tokens

    target_words = ['reference', 'funding', 'awknowledge', 'bibliography']
    conclusion_end_idx = []

    for word in target_words:
        if word in conclusion:
            position = conclusion.index(word)
            conclusion_end_idx.append(position)

    index_of_small = len(processed_tokens)

    if len(conclusion_end_idx) > 0:
        index_of_small = conclusion_end_idx.index(min(filter(lambda x: x > 0, conclusion_end_idx)))

    conclusion = conclusion[0:conclusion_end_idx[index_of_small]]
    sentences = nltk.pos_tag(conclusion)
    grammar = r"""
        BASE: {<DT>?<NN.*><VB.*>}
	    PRESENT: {<BASE>+<DT>?<NN.*|VB.*|JJ.*>?}
        PROPOSE: {<PRP><VB.*>*<NN.*>} 
	    NP: { <PRESENT> | <PROPOSE> }
    """
    p = nltk.RegexpParser(grammar)
    res = p.parse(sentences)

    conclusion_parse_res = []

    for subtree in res.subtrees(filter=lambda t: t.label() == 'NP'):
        conclusion_parse_res.append(str(subtree))

    return conclusion_pos, len(processed_tokens), conclusion_parse_res


# get document from title to references
def get_main_text(document):
    # Разделение текста на предложения
    sentences = nltk.sent_tokenize(document)
    main_text = ""

    end_index = 0
    tw = ['funding', 'acknowledge', 'bibliography', 'references']
    i = 0

    for sentence in sentences:
        for k in tw:
            a = k in sentence
            if a and i > end_index:
                end_index = i
        i = i + 1

    main_text = " ".join(sentences) if end_index == 0 else " ".join(sentences[0:end_index])
    # return joint text and the number of the last sentence
    return main_text, end_index


def extract_data(filepath):
    file_name = filepath
    file_folder = ""
    # if (filepath != "test"):
    file_name = os.path.basename(filepath)
    print(f"{file_name}: ")
    file_folder = os.path.dirname(filepath)
    start_time = time.time()
    
    # if (filepath != "test"):
    text_file = open(filepath, mode="r", encoding="utf-8")
    document = text_file.read()  # open a document
    text_file.close()
    
    # document = """Fig. 1. Typical example of ISM-simulated RIR, computed for a 6.6hz and 3 m2 reverberation 0:3 s (the ordinate axis has been cropped for display purposes)."""

    # remove all non-printable symbols
    document = re.sub(r'[^{0}\n]'.format(string.printable), '', document)
    main_text = get_main_text(document)[0]
    significant_sents = DictionaryManager.filter_sentences(main_text)
    d = DictionaryManager.highlight_domain_words(main_text)
    d = DictionaryManager.highlight_theory_words(d)

    words_filtration_sentences = open(os.path.join(file_folder, f"{file_name}-sent.txt"), "w", encoding="utf-8")
    for w in significant_sents:
       words_filtration_sentences.write(w + '\n')
    words_filtration_sentences.close()

    tokens = nltk.word_tokenize(" ".join(significant_sents))
    processed_tokens = stem(tokens)
    sentences = nltk.pos_tag(processed_tokens)

    grammar = r"""
        ABBREVIATION: {<NN><\(><[A-Z]*?><\)>}  
     	NUMBERL: {<CD><NN.*|VBD>?}
        STANDARD: {<NN.*|VB.*><CD>}
        NECESSARY: {<PRP><VB.*><JJ><TO><VB.*><DT>?<JJ>?<NN.*>*}
        BINARY_COMPARE: {<NN.*>+<VBZ><RBR><IN><VB.*>} 
     	NP: { <ABBREVIATION>  | <BINARY_COMPARE> | <NECESSARY> | <NUMBERL><VBD>?<NUMBERL><VBD>?<NUMBERL> | <NUMBERL><VBD?><NUMBERL> | <NUMBERL>+ | <STANDARD> | <NECESSARY>| <NUMBERR><VBD>?<NUMBERR><VBD>?<NUMBERR> | <NUMBERR><VBD?><NUMBERR> | <NUMBERR>+ }
    """

    # # 1800 m3 parsed

    p = nltk.RegexpParser(grammar)
    res = p.parse(sentences)

    parse_result = open(os.path.join(file_folder, f"{file_name}-result.txt"), "w", encoding="utf-8")

    # # extracting NP tagged chunks
    m_subtrees = res.subtrees(filter=lambda t: t.label() == 'NP')
    np_trees_unique = []
    np_trees = []

    # # transform generators to list
    for subtree in m_subtrees:
        np_trees.append(deepcopy(subtree))
    
    trigger_words = DictionaryManager.get_trigger_rules()
    np_trees_unique = RuleExecutor.filter_nodes_by_tag('NUMBERL', np_trees)
    # lambdas = RuleExecutor.get_trigger_lambdas('numbered_entities', trigger_words)
    # lambda_args = RuleExecutor.get_trigger_lambda_args('numbered_entities', trigger_words)
    
    nn = re.sub(r'[^\w]', '', np_trees_unique[0][0][0][0]) == ''
    #write_trees(RuleExecutor.rule_exec(np_trees_unique, lambdas, lambda_args), parse_result)
    write_domain_entities(RuleExecutor.all_rules_exec(np_trees_unique, trigger_words), parse_result)

    # np_trees_unique = filter_nodes_by_tag('ABBREVIATION', np_trees)
    # np_trees_unique = [t for t in np_trees_unique if t[0][1][0] == '(' or t[0][3][0] == ')']
    # write_trees(np_trees_unique, parse_result)

    # np_trees_unique = filter_nodes_by_tag('BINARY_COMPARE', np_trees)
    # write_trees(np_trees_unique, parse_result)

    # np_trees_unique = filter_nodes_by_tag('NECESSARY', np_trees)
    # write_trees(np_trees_unique, parse_result)

    # parse_result.close()

    parse_result.write("\n------\n")
    statistic_count = 0
    geometric_count = 0
    wave_count = 0
    
    for fw in d[1]:
        s = "Found word! " + fw[1] + " - theory - " + fw[0] + "\n"
        parse_result.write(s)
        if fw[0] == "statistical":
            statistic_count = statistic_count + 1
        if fw[0] == "geometric":
            geometric_count = geometric_count + 1
        if fw[0] == "wave":
            wave_count = wave_count + 1
    
    parse_result.write(f"Statistical - {statistic_count}, geometric - {geometric_count}, wave - {wave_count}\n")

    end_time = time.time()

    elapsed_time = end_time - start_time
    print('Elapsed time: ', elapsed_time)
