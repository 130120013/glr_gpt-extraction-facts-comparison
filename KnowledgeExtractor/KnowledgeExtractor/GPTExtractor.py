from pickle import FALSE
import requests
import uuid
import json

def get_token():
    url = "https://ngw.devices.sberbank.ru:9443/api/v2/oauth"

    payload='scope=GIGACHAT_API_PERS'
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'RqUID': str(uuid.uuid4()),
      'Authorization': 'Basic ZDNkMDY2MTMtNjE3ZS00ZjhiLWEzM2UtZjg2NmM1NzhhYjUxOjFlMzYzZjQ0LTVmODUtNDY4OS1iYTQ1LTNiNWI5OWZmNWZiZg=='
    }

    response = requests.request("POST", url, headers=headers, data=payload, verify='C:\\python\\39\\lib\\site-packages\\certifi\\russian_trusted_root_ca.cer')

    #print(response.text)
    return response.json()["access_token"]


def get_response(query, token):
    url = "https://gigachat.devices.sberbank.ru/api/v1/chat/completions"

    auth = 'Bearer ' + token
    payload = json.dumps({
      "model": "GigaChat:latest",
      "messages": [
        {
          "role": "user",
          "content": "Hello, how are you?"
        }
      ],
      "temperature": 1,
      "top_p": 0.1,
      "n": 1,
      "stream": False,
      "max_tokens": 512,
      "repetition_penalty": 1
    })
    headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': auth
    }

    response = requests.request("POST", url, headers=headers, data=payload, verify='C:\\python\\39\\lib\\site-packages\\certifi\\russian_trusted_root_ca.cer')
    # headers = {"Authorization": f"Bearer {token}"}
    # response = requests.get(f"https://api.sber.ai/api/dialogic/v1/chat/generate?text={query}", headers=headers, verify=False)
    return response.json()

token = get_token()
response = get_response("Hello, how are you?", token)
print(response)