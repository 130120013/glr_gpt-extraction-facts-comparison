Промпт:

Проанализируй следующий текст: "
EXPERIMENTAL STUDY OF ACOUSTIC PARAMETERS
HALL ACCORDING TO INTERNATIONAL STANDARD ISO 3382
Tatarnikov Leonid Alekseevich
FEFU, Polytechnic Institute, Department of Electronics, Telecommunications and
instrumentation
Scientific supervisor: Mirgorodskaya Yu.V.
Key words: acoustic parameters of the hall, reverberation time, levels
sound pressure.
Acoustic calculation is necessary to create an environment in the room,
suitable for listening to music or speech. It all comes down to creating the necessary
values of absorption funds, on which the values of reverberation time and
sound pressure levels.
The location of the experiment is the campus of the Far Eastern Federal
University, located in Vladivostok, o. Russian. As an object
The Blue concert hall was chosen for the study. This building represents
is a semicircular room with a stage.
There are 900 seats for listeners, equipped with chairs with the arrangement -
amphitheater, with small writing tables built into the armrests. Seats
upholstered in fabric with a porous filler, during the performance they are unfolded for
the spectators are seated and then folded up.
There is a curtain on the stage, the material for which is used
velvet. Other decorations and any other obstacles or restrictions
There are no surfaces on the stage or in the hall.
The ceiling is made of large plasterboard plates in a wave-like shape,
in the center there is a “pocket” at a height of 9 m from the listeners’ seats, and the walls are presented in
in the form of elongated panels made of chipboard material, the area is marked in blue
walls where perforated chipboard panels are used.
The hall uses a concentrated sound system, presented in the form of three
adjacent speakers: two of them are 12-inch McCauley ID 4.212-
96; one 15-inch center speaker McCauley ID 4.215-5B.
The location of the speakers is marked with red crosses (Fig. 1),
located at a height of 5 meters from the stage level. To take readings as
The measuring device was chosen to be the Ecophysics 110V sound level meter. Level measurements
296
sound pressure and reverberation time were carried out at the points marked in green
cross (Fig. 1), at a height of 1.5 m, which corresponds to the level of the heads of spectators located in
sitting position. Since the hall is symmetrical, measurements were taken from only one of it.
sides.
Rice. 1 – Model of the “Blue” hall, top view
One of the most important indicators of the acoustic properties of a room is considered
reverberation time - the time required for the average sound energy density to decay
60 dB from its original level after turning off the sound source. Measuring time
reverberation can be produced at any or all fill levels
premises. If the room is equipped with technical means to change it
acoustic characteristics, it is advisable to carry out measurements separately for each
technical means with its normal settings. Temperature and relative
indoor air humidity should be measured with an accuracy of ±1°С and ±5%
respectively.
Rice. 2 – Model of the “Blue” hall, side view
297
During the experiment, an almost complete absence of any
or strangers other than the initiators themselves and the equipment assistant.
The room is not equipped with means of changing its acoustic characteristics, and
temperature and relative humidity were between 20℃ and 45%
respectively.
Reverberation time measurements were carried out in two stages: first - source
the sound signal is located in the center; the second is the source of the sound signal
located on the side. Pink noise was chosen as the sound signal.
For the experiment, 24 points were chosen, distributed evenly in the left
parts of the hall from bottom to top, then, to average the experimental values, it was decided to split
all points into groups of 4 and 10 pieces. Measurements were carried out at a height of 1.5 m, at the level
the heads of seated listeners using a sound level meter. The measurement results have been processed
in the Signal+ program in the octave frequency range and are presented in Table 1.
Table 1
Reverberation time
frequency Hz
Reverberation time, s
Sound source from side Sound source from side
center
1-10
points
11-20
points
21-24
points
1-10 points 11-20
points
21-24
points
63 1.53 1.43 1.4 1.53 1.46 1.32
125 1.53 1.64 1.6 1.53 1.67 1.62
250 1.67 1.55 1.55 1.67 1.58 1.53
500 1.5 1.55 1.56 1.51 1.59 1.49
1000 1.59 1.63 1.57 1.57 1.57 1.59
2000 1.6 1.46 1.5 1.59 1.47 1.45
4000 1.45 1.31 1.21 1.4 1.34 1.1
8000 1.59 0.93 0.86 1.5 1.05 1.2
To measure sound pressure levels, 3 rows of 4 seats per
each, similar to the places from the experiment with determining the reverberation time.
The measurement results are presented in Table 2.
table 2
Sound pressure levels
Frequency,
Hz
Sound pressure levels, dB
Sound source from side Sound source from side
center
1st row 11th row 22nd row 1st row 11th row 22nd row
125 84.7 79.3 72.8 68.8 85.4 70.8
250 86
.4 80.1 68.5 76.6 85.5 83.4
500 89.8 85.7 66 89.5 85.5 88.3
1000 87.4 86 88.9 93.9 82.2 92.4
2000 84.7 88.8 94.2 85.9 85.8 87.4
4000 81.6 79.3 84.2 82.1 84.6 77.5
6000 76.3 81.3 76.3 83.2 74.6 77.7
As a result of the experiment, it can be seen that the average reverberation time
is 1.47 s. For a hall with a volume of 5600 m
3 The optimal reverberation time is determined
according to the following formula:
298
𝑇opt = 0.5 ∙ log 𝑉 − 0.3 = 0.5 ∙ log 5600 − 0.3 = 1.57 s, (1)
where 𝑉 is the volume of the room.
The experimental reverberation time turned out to be less than the optimal value
for a given volume of the hall, it follows that it is necessary to reduce the total fund
absorption.
Based on the results of measurements of sound pressure levels, it is necessary to reduce
height of suspension of sound systems, because pressure level is less than required.
BIBLIOGRAPHY

Davydov V.V. Acoustics of premises, St. Petersburg, Institute of Cinema and
television, 1995 - 95 pp.
GOST R ISO 3382-1-2013 Acoustics. Acoustic measurements
premises. Part 1. Auditoriums. Enter. 2014-12-01.
Mankovsky V. S. Acoustics of studios and halls for sound reproduction. - Moscow:
Art, 1966. - 374 p.
M. Yu. Lane, V. N. Sukhov. Acoustic measurements in concert halls with
using different test signals. Scientific and technical production
Construction magazine, 2016.
"
________________________
Ответ:

Цель исследования:

Оценить акустические параметры концертного зала "Blue" в соответствии с международным стандартом ISO 3382.

Методы исследования:

Измерение времени реверберации
Измерение уровней звукового давления
Объект исследования:

Концертный зал "Blue" на территории Дальневосточного федерального университета.

Результаты исследования:

Время реверберации:
Среднее время реверберации составляет 1,47 с.
Оптимальное время реверберации для зала такого объема должно быть 1,57 с.
Уровни звукового давления:
Уровни звукового давления оказались ниже требуемых.
Выводы:

Необходимо уменьшить общее поглощение звука в зале, чтобы увеличить время реверберации.
Необходимо снизить высоту подвески акустических систем, чтобы повысить уровень звукового давления.
Рекомендации:

Уменьшить поглощающие свойства материалов в зале.
Переустановить акустические системы на более низкой высоте.
________________________
Промпт:
Выдели факты о помещении

ОТВЕТ:
Факты о помещении концертного зала "Blue":

Полукруглая форма с расположением сцены в центре.
900 мест для слушателей, оборудованных креслами с откидными столиками.
Кресла обиты тканью с пористым наполнителем.
На сцене имеется занавес из бархата.
Отсутствуют другие препятствия или украшения на сцене или в зале.
Потолок состоит из больших гипсокартонных плит волнообразной формы с "карманом" в центре на высоте 9 м от мест слушателей.
Стены выполнены из удлиненных панелей из ДСП, часть стен облицована перфорированными панелями из ДСП.
Имеется концентрированная звуковая система, состоящая из трех смежных динамиков.
Динамики расположены на высоте 5 метров от уровня сцены.
Объем помещения составляет 5600 м³.
Температура и относительная влажность во время эксперимента составляли 20℃ и 45% соответственно.
________________________
Промпт:
Выдели заключение

ОТВЕТ:
Заключение:

Проведенные измерения показали, что время реверберации в концертном зале "Blue" составляет 1,47 с, что меньше оптимального значения 1,57 с для зала такого объема. Это означает, что необходимо уменьшить общее поглощение звука в зале.

Уровни звукового давления также оказались ниже требуемых. Для устранения этой проблемы рекомендуется снизить высоту подвески акустических систем.

Таким образом, для улучшения акустических характеристик концертного зала "Blue" необходимо:

Уменьшить поглощающие свойства материалов в зале.
Переустановить акустические системы на более низкой высоте.